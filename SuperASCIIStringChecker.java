import java.io.*;

public class SuperASCIIStringChecker {

	public static void main(String args[]) {
			
		System.out.print("Enter How many Strings : ");
		try{
			
			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
			int numberOfString = Integer.parseInt(bufferReader.readLine());
			char alphabetCharacter[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
			
			mainLoop:
			while(numberOfString!=0) {
				System.out.print("Enter Your String : ");
				String strMessage = bufferReader.readLine();
				int countStringLength = strMessage.length();
				int counterCharacter[] = new int[26];
				counterCharacter[0] =0;
				
				
				for(int j=0;j<alphabetCharacter.length;j++) {
				
					for(int i=0;i<countStringLength;i++) {
						if( (alphabetCharacter[j]+"").equals(strMessage.charAt(i)+"") )
							counterCharacter[j]++;
					}
		
				}
				int flag=0;
				for(int j=0;j<26;j++) 
					if(counterCharacter[j]!=0) {
						if(j != (counterCharacter[j]-1))	
							flag=1;
				}
				if(flag==1)
					System.out.println("No");
				else
					System.out.println("Yes");
				numberOfString--;
			}
		
		}catch(IOException exception) {
			System.out.println(exception);
		}catch(NumberFormatException exception){
			System.out.println(exception);
		}
	}
}