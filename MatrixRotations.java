import java.io.*;

public class MatrixRotations {
	
	static int operationCount=0;

	public static void main(String args[]) {
		
		System.out.print("Enter Size of The Matrix : ");
		BufferedReader bufferedReader=null;
		int numberOfMatrixSize=0;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			numberOfMatrixSize = Integer.parseInt(bufferedReader.readLine());
		}catch(IOException e) {
			System.out.println(e);
		}
		int []createMatrix[] = new int[numberOfMatrixSize][numberOfMatrixSize];
		int []bufferedMatrix[] = new int[numberOfMatrixSize][numberOfMatrixSize];
		
		System.out.println("**********Initial Matrix is**********");
		for(int i=0,arrayValue=1;i<numberOfMatrixSize;i++)
			for(int j=0;j<numberOfMatrixSize;j++)
				createMatrix[i][j] = arrayValue++;
		
		for(int i=0;i<numberOfMatrixSize;i++) {
			for(int j=0;j<numberOfMatrixSize;j++)
				System.out.printf("%5d",createMatrix[i][j]);
			System.out.println("");
		}
		System.out.println("**********Enter Your Operation**********");
		System.out.println("Syntax of Enter Operation Values<A-Angle>,<Q-query>,<U-update>");
		System.out.println("A degree like A 90");
		System.out.println("Q Row Cols like Q 1 1");
		System.out.println("U Row Cols UpdatedValue like U 1 1 6");
		System.out.println("-1 then Exit Program");
		String operationString="";
		
		while(true) {
		
			try{
				operationString  = bufferedReader.readLine();
			}catch(IOException e){
				System.out.println(e);
			}
			
			String cutOperation[] = operationString.split(" ");
			
			if( (cutOperation.length>=2 || cutOperation.length<=4) || (operationString.equals("-1")) ) {
			
				switch(cutOperation[0].toUpperCase()) {
					case "A":
						if(operationCount>=360) 
							operationCount=0;
	
						operationCount += Integer.parseInt(cutOperation[1]);					
						switch( (operationCount+"") ) {
							case "90":
								operationCount=0;
								for(int i=0,x=0;i<numberOfMatrixSize;i++,x++) {
									for(int j=(numberOfMatrixSize-1),y=0;j>=0;j--,y++) {
										bufferedMatrix[x][y]=createMatrix[j][i];
										System.out.printf("%5d",createMatrix[j][i]);
									}
									System.out.println();
								}break;
							case "180":
								operationCount=0;
								for(int i=(numberOfMatrixSize-1),x=0;i>=0;i--,x++) {
									for(int j=(numberOfMatrixSize-1),y=0;j>=0;j--,y++) {
										bufferedMatrix[x][y]=createMatrix[i][j];
										System.out.printf("%5d",createMatrix[i][j]);
									}
									System.out.println();
								}break;
							case "270":
								operationCount=0;
								for(int i=(numberOfMatrixSize-1),x=0;i>=0;i--,x++) {
									for(int j=0,y=0;j<=(numberOfMatrixSize-1);j++,y++) {
										bufferedMatrix[x][y]=createMatrix[j][i];
										System.out.printf("%5d",createMatrix[j][i]);
									}
									System.out.println();
								}break;
							case "360":
								operationCount=0;
								for(int i=0,x=0;i<numberOfMatrixSize;i++,x++) {
									for(int j=0,y=0;j<numberOfMatrixSize;j++,y++) {
										bufferedMatrix[x][y] = createMatrix[i][j];
										System.out.printf("%5d",createMatrix[i][j]);
									}
									System.out.println();
								}break;
							default:
								System.out.println("Operation Syntax Error"+operationCount);
						}
						break;
					case "Q": 
						try {
							int row  = Integer.parseInt(cutOperation[1]);
							int cols = Integer.parseInt(cutOperation[2]);
							System.out.println(bufferedMatrix[row-1][cols-1]);
						}catch(ArrayIndexOutOfBoundsException e) {
							System.out.println("Position of Rows and Cols are Wrong");
						}catch(NumberFormatException e) {
							System.out.println("Value of Rows and Cols Only Positive Numeric");
						}
						break;
					case "U":
						try{
							int row  = Integer.parseInt(cutOperation[1]);
							int cols = Integer.parseInt(cutOperation[2]);
							createMatrix[row-1][cols-1] = Integer.parseInt(cutOperation[3]);
							System.out.println("**********Updated Initial Matrix is**********");
							for(int i=0;i<numberOfMatrixSize;i++) {
								for(int j=0;j<numberOfMatrixSize;j++)
									System.out.printf("%5d",createMatrix[i][j]);
								System.out.println("");
							}
						}catch(ArrayIndexOutOfBoundsException e) {
							System.out.println("Your Position Not Found");
						}catch(NumberFormatException e) {
							System.out.println("Invalid Parameter : "+e.getMessage());
						}
						break;
					case "-1":
						System.exit(0);
					default:
						System.out.println("Operation name Wrong");
				}
				
			}else
				System.out.println("Operation Syntax Wrong");
				
		}
	}
}